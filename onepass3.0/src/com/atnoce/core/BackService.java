/*
 * Copyright 2016 atnoce.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.atnoce.core;

import com.atnoce.util.Cache;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.scene.control.Button;

/**
 *
 * @author atnoce.com
 */
public class BackService {
    private BackService(){
        
    }
    private static BackService backService;
    private static long time=1476960799526l;
    
    public static void updateClock(){
        time = System.currentTimeMillis();
    }
    public static void startClockService(Button btn){
        if(!Boolean.parseBoolean(Cache.oc.getKongXianLock()))return;
        time = System.currentTimeMillis();
      
        Service<String> service=new Service<String>() {
            @Override
            protected Task<String> createTask() {
                return new Task<String>() {
                    @Override
                    protected String call() throws Exception {
                        long currentTimeMillis = System.currentTimeMillis();
                        while((currentTimeMillis-time)/1000/60<Long.parseLong(Cache.oc.getKongXianLockTime())){
                            currentTimeMillis = System.currentTimeMillis();
                            
                            Thread.sleep(1000);
                        }
                        return "end";
                    }
                };
            }
        };
        service.setOnSucceeded((WorkerStateEvent event) -> {
            btn.fire();
        });
    
        service.start();
    }
}
