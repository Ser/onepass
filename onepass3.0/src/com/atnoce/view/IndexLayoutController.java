/*
 * Copyright 2016 atnoce.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.atnoce.view;

import com.atnoce.Start;
import com.atnoce.cloud.ColudController;
import com.atnoce.core.BackService;
import com.atnoce.core.SecurityController;
import com.atnoce.db.ClassTypeDao;
import com.atnoce.db.CommonDao;
import com.atnoce.db.DbUtils;
import com.atnoce.db.PasswordDao;
import com.atnoce.pojo.DirType;
import com.atnoce.pojo.PasswordItem;
import com.atnoce.util.Cache;
import com.atnoce.util.CommonUtil;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import javafx.application.Platform;

import org.apache.commons.lang.StringUtils;


import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.Clipboard;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

public class IndexLayoutController {

    @FXML
    private ListView<String> classType;
    @FXML
    private TableView<PasswordItem> tableView;
    @FXML
    private TableColumn<PasswordItem, String> accountColumn;
    @FXML
    private TableColumn<PasswordItem,String> remarkStrColumn;
    @FXML
    private Button addDirTypeBtn;
    @FXML
    private Button addPassItemBtn;
    @FXML
    private Button optionsBtn;
    @FXML
    private Button helpBtn;
    @FXML
    private TextField showUsername;
    @FXML
    private TextField showPass;
    @FXML
    private TextArea showBeizhu;
    @FXML
    private Button copyUsernameBtn;
    @FXML
    private Button copyPassBtn;
    @FXML 
    private TextField searchInput;
    @FXML
    private Button searchBtn;
    @FXML
    private Button lockBtn;
    @FXML
    private ImageView coludImg;
    @FXML
    private Button coludBtn;

    private Start start;
    private ObservableList<DirType> dirTypes=null;
     private final ObservableList<String> ol=FXCollections.observableArrayList();
    private ObservableList<PasswordItem> pitems=null;
    private final ClassTypeDao ctd=new ClassTypeDao();
    private final PasswordDao pd=new PasswordDao();
    private final CommonDao cd=new CommonDao();
    
    public void setStart(Start start){
        this.start=start;
    }
    @FXML
    private void initialize(){
        
	initTable();
	initLeftList();

	initBtnEventHandle();

	initShowPanel();
	classType.getSelectionModel().select(0);
        
        showBeizhu.setWrapText(true);
        
        searchInput.setOnKeyReleased(e->search());
        initTip();
        
        BackService.startClockService(lockBtn);
        
       
    }
    private void initTip(){
        addDirTypeBtn.setTooltip(CommonUtil.getTooltip("添加分类", 14));
        addPassItemBtn.setTooltip(CommonUtil.getTooltip("添加密码项", 14));
        optionsBtn.setTooltip(CommonUtil.getTooltip("系统全局配置", 14));
        helpBtn.setTooltip(CommonUtil.getTooltip("获取帮助信息", 14));
        lockBtn.setTooltip(CommonUtil.getTooltip("锁定主界面", 14));
        coludBtn.setTooltip(CommonUtil.getTooltip("立即进行同步", 14));
        searchInput.setTooltip(CommonUtil.getTooltip("输入任意字符全局搜索结果", 14));
        classType.setTooltip(CommonUtil.getTooltip("右键可显示更多菜单",14));
        tableView.setTooltip(CommonUtil.getTooltip("双击在下方显示密码信息，鼠标右键点击显示更多菜单", 14));
        showUsername.setTooltip(CommonUtil.getTooltip("双击复制账号", 14));
        showPass.setTooltip(CommonUtil.getTooltip("双击复制密码", 14));
        
    }
    private void initShowPanel(){
        showUsername.textProperty().addListener((observable,oldValue,newValue)->{
            copyUsernameBtn.setDisable(StringUtils.isBlank(newValue));
        });
        showPass.textProperty().addListener((observable,oldValue,newValue)->{
            copyPassBtn.setDisable(StringUtils.isBlank(newValue));
        });
        //copyPassBtn.setDisable(showUsername.getProperties().);
    }
        /**
         * 初始化按钮事件
         */
    private void initBtnEventHandle(){
	addDirTypeBtn.setOnAction(e->addDirTypeBtnHandler());
	addPassItemBtn.setOnAction(e->addPassItemBtnHandler(null));
	optionsBtn.setOnAction(e->optionsBtnHandler());
	helpBtn.setOnAction(e->helpBtnHandler());
        lockBtn.setOnAction(e->lockBtnHandler());
        coludBtn.setOnAction(e->execColudSynce());
        copyUsernameBtn.setOnAction(e->copyUsername());
        copyPassBtn.setOnAction(e->copyPassword());
        showUsername.setOnMouseClicked((MouseEvent event)->{
            if(event.getButton().equals(MouseButton.PRIMARY)&&event.getClickCount()==2){
                copyUsername();
            }
        });
        showPass.setOnMouseClicked((MouseEvent event)->{
            if(event.getButton().equals(MouseButton.PRIMARY)&&event.getClickCount()==2){
                copyPassword();
            }
        });
    }
    private void copyUsername(){
        if(StringUtils.isNotBlank(showUsername.getText())){
            showUsername.selectAll();
            showUsername.copy();
        }
    }
    private void copyPassword(){
        if(StringUtils.isNotBlank(showPass.getText())){
             showPass.selectAll();
             showPass.copy();
        }
    }
    private void execColudSynce(){
        if(Boolean.valueOf(Cache.proMap.get("actioncloud"))){
            Platform.runLater(()->{
                coludImg.setImage(new Image(IndexLayoutController.class.getResourceAsStream("image/5-121204193R6.gif")));
                    try {
                        coludBtn.setDisable(true);
                        //关闭数据库;
                        DbUtils.closeAll();
                        //与云服务器联系
                        ColudController colud=new ColudController();
                        Map<String, Object> sync = colud.sync();
                        if((boolean)sync.get("flag")){
                            coludImg.setImage(new Image(IndexLayoutController.class.getResourceAsStream("image/colud_succes.png")));
                        }else{
                            //同步发生问题
                            coludImg.setImage(new Image(IndexLayoutController.class.getResourceAsStream("image/colud_change.png")));
                            Alert alert=new Alert(AlertType.WARNING);
                            alert.setTitle("警告");
                            alert.setHeaderText(null);
                            alert.setContentText("当前计算机未与服务器取得联系!");
                            alert.showAndWait();
                        }
                        coludBtn.setDisable(false);
                    } catch (SQLException ex) {
                        Alert alert=new Alert(AlertType.ERROR);
                        alert.setHeaderText("同步失败");
                        alert.setContentText("同步时发生错误!"+ex.getMessage());
                        alert.showAndWait();
                        coludBtn.setDisable(false);
                }
            });
            
        }
    }
    private void lockBtnHandler(){
        start.initLoginLayout(null);
    }
    private void helpBtnHandler() {
	/*DirectoryChooser chooser=new DirectoryChooser();
        chooser.setTitle("选择数据文件存放路径");
        File showDialog = chooser.showDialog(onepass.getPrimaryStage());
        System.out.println(toJavaFilePath(showDialog.getAbsolutePath()));
        System.out.println(showDialog.getAbsolutePath());*/
       // new ColudController().sync();
       //onepass.showAbout();
       try{
           FXMLLoader loader=new FXMLLoader();
           loader.setLocation(IndexLayoutController.class.getResource("ShowAbout.fxml"));
           BorderPane bp=loader.load();
           
           Stage aboutStage=new Stage();
           aboutStage.setTitle("关于onepass");
           aboutStage.initModality(Modality.WINDOW_MODAL);
           //aboutStage.initStyle(StageStyle.UNDECORATED);
           aboutStage.initOwner(start.getPrimaryStage());
           Scene scene=new Scene(bp);
           aboutStage.setScene(scene);
           aboutStage.showAndWait();
                   
       }catch(IOException e){
       }
           
    }
    public static String toJavaFilePath(String path) {
        String FILE_SEPERATOR  = "/";
        if (path == null) {
            return null;
        }
        path = StringUtils.replace(path, "//", FILE_SEPERATOR);
        path = StringUtils.replace(path, "////", FILE_SEPERATOR);
        path = StringUtils.replace(path, "//////", FILE_SEPERATOR);
        path = StringUtils.replace(path, "////////", FILE_SEPERATOR);
        path = StringUtils.replace(path, "/", FILE_SEPERATOR);
        path = StringUtils.replace(path, "//", FILE_SEPERATOR);
        path = StringUtils.replace(path, "///", FILE_SEPERATOR);
        path = StringUtils.replace(path, "////", FILE_SEPERATOR);
        path = StringUtils.replace(path, "${FILE_SEPERATOR}", FILE_SEPERATOR);
        return path;
    }
        /**
         * 系统配置按钮处理函数
         */
    private void optionsBtnHandler() {
		// TODO Auto-generated method stub
	start.showPreferencesDialog();
    }
        /**
         * 添加密码按钮处理函数
         */
    private void addPassItemBtnHandler(PasswordItem passwordItem) {
		// TODO Auto-generated method stub
	PasswordItem pi=passwordItem==null?new PasswordItem():passwordItem;
        Cache.INDEX_LEFT_SELECTED_DIRTYPE = classType.getSelectionModel().getSelectedItem();
	boolean okClicked = start.showPassEditDialog(pi);
	if(okClicked){
            String um = null;
            try {
                um = SecurityController.getSecurityInstence().AESdecrypt(pi.getAccount(), Cache.key);
            } catch (Exception ex) {
                Alert alert=new Alert(AlertType.ERROR);
                alert.setHeaderText("添加失败");
                alert.setContentText("添加密码项目时发生错误："+ex.getMessage());
                alert.showAndWait();
                return;
            }
            coludImg.setImage(new Image(IndexLayoutController.class.getResourceAsStream("image/colud_change.png")));
            pi.setAccount(um);
            tableView.getItems().add(pi);
            ObservableList<String> items = classType.getItems();

            for(int a=0;a<items.size();a++){
		if(StringUtils.equals(items.get(a), Cache.ADD_PASS_ITEM_SELECT_DIRTYPE)){
                    classType.getSelectionModel().select(a);
                    break;
		}
            }
        }
	Cache.ADD_PASS_ITEM_SELECT_DIRTYPE=null;
        Cache.INDEX_LEFT_SELECTED_DIRTYPE=null;
    }
        /**
         * 添加分类按钮处理函数
         */
    private void addDirTypeBtnHandler() {
		// TODO Auto-generated method stub
	TextInputDialog dialog = new TextInputDialog("");
	dialog.setTitle("添加分类");
	dialog.setHeaderText(null);
	dialog.setContentText("请输入分类名称:");

	Optional<String> result = dialog.showAndWait();

	result.ifPresent(name -> {
            if(StringUtils.isNotBlank(name.trim())){
		try {
                    DirType dirType = new DirType(name.trim());
                    Map<String, Object> addres = ctd.addDirType(dirType);
                    if((boolean)addres.get("flag")){
                        ol.add(dirType.getDirName());
                        dirTypes.add(dirType);
                        coludImg.setImage(new Image(IndexLayoutController.class.getResourceAsStream("image/colud_change.png")));
                    }else{
                        Alert alert=new Alert(AlertType.ERROR);
			alert.setTitle("添加失败");
			alert.setHeaderText("对不起，分类添加失败了!");
			alert.setContentText((String)addres.get("msg"));
			alert.showAndWait();
                    }
		} catch (Exception e) {
                        Alert alert=new Alert(AlertType.ERROR);
			alert.setTitle("添加失败");
			alert.setHeaderText("对不起，分类添加失败了!");
			alert.setContentText("添加分类发生系统错误！");
			alert.showAndWait();
		}
            }
	});
    }
   
    private void initLeftList(){
	try {
            dirTypes = ctd.getDirTypes();
            for (DirType dirType : dirTypes) {
                ol.add(dirType.getDirName());
            }
            classType.setItems(ol);
	} catch (SQLException e) {
		Alert alert=new Alert(AlertType.ERROR);
		//alert.setTitle("添加失败");
		alert.setHeaderText("初始化分类列表错误!");
		alert.setContentText("读取分类列表发生异常！");
		alert.showAndWait();
	}catch(ClassNotFoundException e){
            Alert alert=new Alert(AlertType.ERROR);
		//alert.setTitle("添加失败");
		alert.setHeaderText("初始化分类列表错误!");
		alert.setContentText("找不到数据库驱动");
		alert.showAndWait();
        }
//	classType.setCellFactory((ListView<DirType> arg0) -> new ListCell<DirType>(){
//            public void updateItem(DirType item,boolean empty){
//                super.updateItem(item, empty);
//                if(item!=null&&!empty){
//                    this.setText(item.getDirName());
//                }
//            }
//        });
        classType.getSelectionModel().selectedItemProperty().addListener((ObservableValue<? extends String> ov,
		String old_val,String new_val)->{
                   updateTable(new_val);
        });
        initLiftRightContextMenu();
    }
    public void initLiftRightContextMenu(){
	//为分类初始化右键菜单
	final ContextMenu contextMenu = new ContextMenu();
	contextMenu.setOnShowing((WindowEvent e) -> {
            //System.out.println("showing");
        });
	contextMenu.setOnShown((WindowEvent e) -> {
            //System.out.println("shown");
        });

	MenuItem item1 = new MenuItem("删除分类");
	item1.setOnAction((ActionEvent e) -> {
             if(classType.getItems().size()==1){
                Alert alert=new Alert(AlertType.ERROR);
                alert.setHeaderText("不能删除该分类");
                alert.setContentText("至少要有一个分类！");
                alert.showAndWait();
                return;
            }
            String selectedItem = classType.getSelectionModel().getSelectedItem();
            Alert alert=new Alert(AlertType.CONFIRMATION);
            alert.setHeaderText("确定要删除该分类吗？");
            alert.setContentText("删除分类会将该分类下的所有密码项删除!");
            Optional<ButtonType> showAndWait = alert.showAndWait();
            if(showAndWait.get().getButtonData()==ButtonData.OK_DONE){
                try {
                    //执行删除分类逻辑
                    //删除子项
                    pd.deletePasswordItemFromDirType(selectedItem);
                    //删除分类
                    if(ctd.deleteDirType(selectedItem)){
                         ol.remove(selectedItem);
                         DirType dt=null;
                         for(DirType dirType:dirTypes){
                             if(StringUtils.equals(dirType.getDirName(), selectedItem)){
                                 dt=dirType;
                                 break;
                             }
                         }
                         if(dt!=null)
                         dirTypes.remove(dt);
                    }
                    
                } catch (SQLException ex) {
                    alert=new Alert(AlertType.ERROR);
                    alert.setHeaderText("删除分类失败");
                    alert.setContentText("系统删除分类时发生错误！");
                    alert.showAndWait();
                }catch(ClassNotFoundException ex){
                    alert=new Alert(AlertType.ERROR);
                    alert.setHeaderText("删除分类失败");
                    alert.setContentText("找不到数据库驱动！");
                    alert.showAndWait();
                }
            }
        });
	MenuItem item2 = new MenuItem("添加记录");
	item2.setOnAction((ActionEvent e) -> {
            addPassItemBtnHandler(null);
        });
        MenuItem item3 = new MenuItem("重命名");
	item3.setOnAction((ActionEvent e) -> {
            String selectedItem = classType.getSelectionModel().getSelectedItem();
            if(selectedItem==null){
                Alert alert=new Alert(AlertType.WARNING);
                alert.setHeaderText("操作无效！");
                alert.setContentText("请将鼠标放在要修改的密码项上再点击鼠标右键");
                alert.show();
                return;
            }
            TextInputDialog dialog = new TextInputDialog(selectedItem);
            dialog.setTitle("重命名");
            dialog.setHeaderText("分类名称不能超过20个字符");
            dialog.setContentText("请输入新的分类名称:");

            Optional<String> result = dialog.showAndWait();

            result.ifPresent(name -> {
                if(StringUtils.isNotBlank(name.trim())&&!StringUtils.equals(selectedItem, name)){
                    if(name.trim().length()>20){
                        Alert alert=new Alert(AlertType.ERROR);
                        alert.setHeaderText("重命名失败");
                        alert.setContentText("分类名称不能大于8个字符");
                        alert.showAndWait();
                        return;
                    }
                    try {
                        if(ctd.renameDirType(selectedItem, name.trim())){
                            classType.getItems().remove(classType.getSelectionModel().getSelectedIndex());
                            classType.getItems().add(classType.getSelectionModel().getSelectedIndex()+1, name.trim());
                        }
                    } catch (SQLException ex) {
                        Alert alert=new Alert(AlertType.ERROR);
                        alert.setHeaderText("重命名失败");
                        alert.setContentText("重命名分类名称时发生系统错误！");
                        alert.showAndWait();
                    }catch(ClassNotFoundException ex){
                        Alert alert=new Alert(AlertType.ERROR);
                        alert.setHeaderText("重命名失败");
                        alert.setContentText("找不到数据库驱动！");
                        alert.showAndWait();
                    }
                }
            });
            
        });
	contextMenu.getItems().addAll(item2,item3, item1);
        classType.setContextMenu(contextMenu);
            //给tableView添加右键菜单
            //tableView.setContextMenu(contextMenu);
    }
    public void initPassItemRightContextMenu(){
        final ContextMenu contextMenu = new ContextMenu();
        MenuItem delete = new MenuItem("删除");
        delete.setOnAction((ActionEvent e) -> {
           
            PasswordItem selectedItem = tableView.getSelectionModel().getSelectedItem();
            if(selectedItem==null){
                Alert alert=new Alert(AlertType.WARNING);
                alert.setHeaderText("操作无效！");
                alert.setContentText("请将鼠标放在要修改的密码项上再点击鼠标右键");
                alert.show();
                return;
            }
                Alert alert=new Alert(AlertType.CONFIRMATION);
                alert.setHeaderText("确定要删除么？");
                alert.setContentText("密码条目删除后无法恢复！");
                Optional<ButtonType> showAndWait = alert.showAndWait();
                if(showAndWait.get().getButtonData()==ButtonData.OK_DONE){
                    try {
                        //执行删除逻辑
                        if(pd.deletePasswordItem(selectedItem)){
                           tableView.getItems().remove(selectedItem);
                        }
                    } catch (SQLException ex) {
                        alert=new Alert(AlertType.ERROR);
                        alert.setHeaderText("删除密码条目失败");
                        alert.setContentText("删除密码条目时发生系统错误！");
                        alert.showAndWait();
                    }catch(ClassNotFoundException ex){
                        alert=new Alert(AlertType.ERROR);
                        alert.setHeaderText("删除密码条目失败");
                        alert.setContentText("找不到数据库驱动！");
                        alert.showAndWait();
                    }
                }
            
        });
        MenuItem edit = new MenuItem("修改");
        edit.setOnAction((ActionEvent e) -> {
            PasswordItem selectedItem = tableView.getSelectionModel().getSelectedItem();
            if(selectedItem==null){
                Alert alert=new Alert(AlertType.WARNING);
                alert.setHeaderText("操作无效！");
                alert.setContentText("请将鼠标放在要修改的密码项上再点击鼠标右键");
                alert.show();
                return;
            }
            try {
                updatePassItemBtnHandler(selectedItem);
            } catch (SQLException ex) {
                Alert alert=new Alert(AlertType.ERROR);
                alert.setHeaderText("修改密码条目失败");
                alert.setContentText("修改密码条目时发生系统错误！");
                alert.showAndWait();
            }catch(ClassNotFoundException ex){
                Alert alert=new Alert(AlertType.ERROR);
                alert.setHeaderText("修改密码条目失败");
                alert.setContentText("找不到数据库驱动！");
                alert.showAndWait();
            }
        });
        
        MenuItem copyAccount = new MenuItem("复制账号");
        copyAccount.setOnAction((ActionEvent e) -> {
            PasswordItem selectedItem = tableView.getSelectionModel().getSelectedItem();
            if(selectedItem==null){
                Alert alert=new Alert(AlertType.WARNING);
                alert.setHeaderText("操作无效！");
                alert.setContentText("请将鼠标放在要修改的密码项上再点击鼠标右键");
                alert.show();
                return;
            }
            TextField textField = new TextField(selectedItem.getAccount());
            textField.selectAll();
            textField.copy();
        });
        
        MenuItem copyPass = new MenuItem("复制密码");
        copyPass.setOnAction((ActionEvent e) -> {
             PasswordItem selectedItem = tableView.getSelectionModel().getSelectedItem();
             if(selectedItem==null){
                Alert alert=new Alert(AlertType.WARNING);
                alert.setHeaderText("操作无效！");
                alert.setContentText("请将鼠标放在要修改的密码项上再点击鼠标右键");
                alert.show();
                return;
            }
            TextField textField = null;
            try {
                textField = new TextField(SecurityController.getSecurityInstence().AESdecrypt(selectedItem.getPasswordStr(), Cache.key));
            } catch (Exception ex) {
                Alert alert=new Alert(AlertType.ERROR);
                alert.setHeaderText("复制失败");
                alert.setContentText("复制密码时发生错误："+ex.getMessage());
                alert.showAndWait();
                return;
            }
            textField.selectAll();
            textField.copy();
        });
        
        
        contextMenu.getItems().addAll(edit,copyAccount,copyPass,delete);
        tableView.setContextMenu(contextMenu);
    }
    private void updatePassItemBtnHandler(PasswordItem passwordItem) throws SQLException, ClassNotFoundException {
		// TODO Auto-generated method stub
	//PasswordItem pi=passwordItem;
        
        PasswordItem pi=pd.getPasswordItemFromPassId(passwordItem.getPassId());
        String _itemDirTypeId=pi.getDirTypeId();
        Cache.INDEX_LEFT_SELECTED_DIRTYPE = classType.getSelectionModel().getSelectedItem();
	boolean okClicked = start.showPassEditDialog(pi);
	if(okClicked){
            PasswordItem selectedItem = tableView.getSelectionModel().getSelectedItem();
            if(!StringUtils.equals(_itemDirTypeId, pi.getDirTypeId())){
                tableView.getItems().remove(selectedItem);
                return;
            }
            String um = null;
            try {
                um = SecurityController.getSecurityInstence().AESdecrypt(pi.getAccount(), Cache.key);
            } catch (Exception ex) {
               Alert alert=new Alert(AlertType.ERROR);
               alert.setHeaderText("更新失败");
               alert.setContentText("更新密码条目时发生错误："+ex.getMessage());
               alert.showAndWait();
               return;
            }
            pi.setAccount(um);
            tableView.getItems().remove(selectedItem);
            tableView.getItems().add(pi);
			//classType.setUserData(CommonUtil.ADD_PASS_ITEM_SELECT_DIRTYPE);
        }else{
            try {
                //修改失败需要还原账号
                pi.setAccount(SecurityController.getSecurityInstence().AESdecrypt(pi.getAccount(), Cache.key));
            } catch (Exception ex) {
               Alert alert=new Alert(AlertType.ERROR);
               alert.setHeaderText("恢复错误");
               alert.setContentText("恢复密码项目出现错误："+ex.getMessage());
               alert.showAndWait();
            }
        }
    }
     ScheduledExecutorService service = Executors.newSingleThreadScheduledExecutor();
    private void initTable(){
	accountColumn.setCellValueFactory(cellData->cellData.getValue().accountProperty());
	remarkStrColumn.setCellValueFactory(cellData->cellData.getValue().remarkStrProperty());
       
	tableView.setOnMouseClicked((MouseEvent event) -> {
            if(tableView.getSelectionModel().getSelectedItem()!=null)
                if(event.getButton().equals(MouseButton.PRIMARY)&&event.getClickCount()==2&&
                    tableView.getSelectionModel().getSelectedIndex()<tableView.getItems().size()){
                        showPassItem(tableView.getSelectionModel().getSelectedItem());
                        if(Boolean.valueOf(Cache.oc.getAutoClearPanel())){
                           /* Runnable runnable = () -> {
                            showPassItem(null);
                            if(!service.isShutdown()){
                                service.shutdown();
                            }
                        };
                        long time=Long.valueOf(Cache.oc.getAutoClearPanelTime());
                            
                        service.schedule(runnable, time, TimeUnit.SECONDS);*/
                        }
                       
                }else{
                    //防止误操作影响其他地方的复制信息
                    if(StringUtils.isNotBlank(showUsername.getText())){
                        Clipboard clipboard=Clipboard.getSystemClipboard();
                        ClipboardContent cc=new ClipboardContent();
                        cc.putString("");
                        clipboard.setContent(cc);
                        showPassItem(null);
                    }
                    
                }
        });
        //初始化列表右键菜单
        initPassItemRightContextMenu();
    }
        /**
         * 查看密码
         * @param pi 
         */
    private void showPassItem(PasswordItem pi){
	if(pi==null){
            showUsername.setText("");
            showPass.setText("");
            showBeizhu.setText("");
	}else{
            showUsername.setText(pi.getAccount());
            try {
                showPass.setText(SecurityController.getSecurityInstence().AESdecrypt(pi.getPasswordStr(), Cache.key));
            } catch (Exception ex) {
                Alert alert=new Alert(AlertType.ERROR);
                alert.setHeaderText("解密失败");
                alert.setContentText(ex.getMessage());
                alert.showAndWait();
                return;
            }
            showBeizhu.setText(pi.getRemarkStr());
        }

    }
        /**
         * 更新密码项目列表
         * @param typeId 分类ID
         */
    private void updateTable(String typeName){
        if(StringUtils.isNotBlank(typeName))
            try {
		pitems = pd.getPasswordsFormDirName(typeName);
                if(pitems!=null){
                    pitems.stream().forEach((p) -> {
                        try {
                            p.setAccount(SecurityController.getSecurityInstence().AESdecrypt(p.getAccount(), Cache.key));
                        } catch (Exception ex) {
                            Alert alert=new Alert(AlertType.ERROR);
                            alert.setHeaderText("更新密码列表失败");
                            alert.setContentText("解密失败！");
                            alert.showAndWait();
                            return;
                        }
                    });
                    tableView.setItems(pitems);
                }
                
            } catch (SQLException e) {
		Alert alert=new Alert(AlertType.ERROR);
                alert.setHeaderText("更新密码列表失败");
                alert.setContentText("获取密码列表时发生系统错误！");
                alert.showAndWait();
            }catch(ClassNotFoundException e){
                Alert alert=new Alert(AlertType.ERROR);
                alert.setHeaderText("更新密码列表失败");
                alert.setContentText("找不到数据库驱动！");
                alert.showAndWait();
            }
    }
    private void search(){
        String str=searchInput.getText().trim();
        
            try {
                ObservableList<PasswordItem> search = cd.search(str);
                for(PasswordItem pi:search){
                    pi.setAccount(SecurityController.getSecurityInstence().AESdecrypt(pi.getAccount(), Cache.key));
                }
                tableView.setItems(search);
            } catch (SQLException ex) {
                Alert alert=new Alert(AlertType.ERROR);
                alert.setHeaderText("查询失败");
                alert.setContentText("全文检索发生错误！");
                alert.showAndWait();
            }catch(ClassNotFoundException e){
                Alert alert=new Alert(AlertType.ERROR);
                alert.setHeaderText("查询失败");
                alert.setContentText("找不到数据库驱动！");
                alert.showAndWait();
            } catch (Exception ex) {
                Alert alert=new Alert(AlertType.ERROR);
                alert.setHeaderText("查询失败");
                alert.setContentText("解密时发生错误:"+ex.getMessage());
                alert.showAndWait();
            }
        
    }
}
