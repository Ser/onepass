/*
 * Copyright 2016 atnoce.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.atnoce.view;

import com.atnoce.Start;
import com.atnoce.db.ConfigDao;
import com.atnoce.pojo.OnepassConfig;
import java.sql.SQLException;

import org.apache.commons.lang.StringUtils;


import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Slider;
import javafx.stage.Stage;

public class PassGenSettingLayoutController {

	private Start start;
	private Stage stage;
	private final ConfigDao cd=new ConfigDao();
	@FXML
	private Slider daZiMu;
	@FXML
	private Slider xiaoZiMu;
	@FXML
	private Slider shuZi;
	@FXML
	private Slider fuHao;
	@FXML
	private Button saveConfig;
	@FXML
	private Button cancle;
	@FXML
	private Slider passLength;
	@FXML
	private CheckBox filterXingTong;
	public void setStart(Start start){
		this.start=start;
	}
	public void setPwdStage(Stage stage){
		this.stage=stage;
	}
	private OnepassConfig onepassConfig;
	@FXML
	private void initialize(){
		saveConfig.setOnAction(e->{
			saveConfigHandler();
		});
		cancle.setOnAction(e->{
			cancleHandler();
		});
		initUiData();
	}
	private void initUiData(){
		try {
			onepassConfig = cd.getOnepassConfig();
			if(onepassConfig!=null){
				daZiMu.setValue(Double.parseDouble(onepassConfig.getStrQuanZhong()));
				xiaoZiMu.setValue(Double.parseDouble(onepassConfig.getXiaoXieStrQuanZhong()));
				shuZi.setValue(Double.parseDouble(onepassConfig.getNumQuanZhong()));
				fuHao.setValue(Double.parseDouble(onepassConfig.getFuhaoQuanZhong()));
				passLength.setValue(Double.parseDouble(onepassConfig.getPassLength()));

				if(StringUtils.equals("true",onepassConfig.getFilterStr())){
					filterXingTong.selectedProperty().setValue(true);
				}else{
					filterXingTong.selectedProperty().setValue(false);
				}
			}
		} catch (SQLException e) {
                    Alert alert=new Alert(Alert.AlertType.ERROR);
                    alert.setHeaderText("配置数据初始化失败");
                    alert.setContentText("初始化配置信息时发生系统错误!");
                    alert.showAndWait();
		}catch(ClassNotFoundException e){
                    Alert alert=new Alert(Alert.AlertType.ERROR);
                    alert.setHeaderText("配置数据初始化失败");
                    alert.setContentText("找不到数据库驱动!");
                    alert.showAndWait();
                }
	}
	private void saveConfigHandler(){
		double d = daZiMu.getValue();
		double x = xiaoZiMu.getValue();
		double s = shuZi.getValue();
		double f = fuHao.getValue();
		double len=passLength.getValue();
		Alert alert;
		if(d+x+s+f<1){
			alert=new Alert(AlertType.ERROR);
			alert.setTitle("生成器配置错误");
			alert.setHeaderText(null);
			alert.setContentText("字母、数字、符号至少要有一个不为0才可以使用密码生成器!");
			alert.showAndWait();
			return;
		}
		if(len<1){
			alert=new Alert(AlertType.WARNING);
			alert.setTitle("生成器配置错误");
			alert.setHeaderText(null);
			alert.setContentText("密码长度至少要有1位!");
			alert.showAndWait();
			return;
		}
		onepassConfig.setStrQuanZhong(String.valueOf((int)d));
		onepassConfig.setXiaoXieStrQuanZhong(String.valueOf((int)x));
		onepassConfig.setNumQuanZhong(String.valueOf((int)s));
		onepassConfig.setFuhaoQuanZhong(String.valueOf((int)f));
		onepassConfig.setPassLength(String.valueOf((int)len));
		onepassConfig.setFilterStr(String.valueOf(filterXingTong.selectedProperty().getValue()));
		try {
			if(cd.savePassGenerConfig(onepassConfig)){
				alert=new Alert(AlertType.INFORMATION);
				alert.setHeaderText(null);
				alert.setTitle("成功");
				alert.setContentText("配置信息保存成功");
				alert.showAndWait();
				stage.close();
			}
		} catch (SQLException e) {
                    alert=new Alert(Alert.AlertType.ERROR);
                    alert.setHeaderText("保存失败");
                    alert.setContentText("保存配置信息时发生系统错误!");
                    alert.showAndWait();
		}catch(ClassNotFoundException e){
                    alert=new Alert(Alert.AlertType.ERROR);
                    alert.setHeaderText("保存失败");
                    alert.setContentText("找不到数据库驱动!");
                    alert.showAndWait();
                }

	}
	private void cancleHandler(){

	}
}
