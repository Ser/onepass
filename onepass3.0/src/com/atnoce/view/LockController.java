/*
 * Copyright 2016 atnoce.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.atnoce.view;

import com.atnoce.Start;
import com.atnoce.core.SecurityController;
import com.atnoce.db.ConfigDao;
import com.atnoce.util.Cache;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import org.apache.commons.lang.StringUtils;

/**
 * FXML Controller class
 *
 * @author Administrator
 */
public class LockController implements Initializable {
    
    @FXML
    private HBox hbox;
    @FXML
    private AnchorPane look1;
    @FXML
    private PasswordField mainPass;
    @FXML
    private Button unlockBtn;
    private final ConfigDao cd=new ConfigDao();
    private Start start;

    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        unlockBtn.setOnAction(e->unlockBtnHandler());
        mainPass.setOnKeyPressed(e->{
			if(e.getCode()==KeyCode.ENTER){
				unlockBtnHandler();
			}
		});
    }    
    public void setStart(Start start){
        this.start=start;
    }
    /**
     * 解锁
     */
    private void unlockBtnHandler(){
        String pass=mainPass.getText();
        if(StringUtils.isBlank(pass)){
            Alert alert=new Alert(Alert.AlertType.ERROR);
            alert.setTitle("解锁失败");
            alert.setHeaderText(null);
            alert.setContentText("密码不能为空!");
            alert.showAndWait();
            return;
        }
         try {
                Cache.oc=cd.getOnepassConfig();
                if(SecurityController.getSecurityInstence().checkMainPass(pass)){
			start.initMainLayout();
		}else{
			Alert alert=new Alert(Alert.AlertType.ERROR);
			alert.setTitle("解锁失败");
			alert.setHeaderText(null);
			alert.setContentText("输入的密码有误!");
			alert.showAndWait();
		}
            } catch (SQLException ex) {
                Alert alert=new Alert(Alert.AlertType.ERROR);
		alert.setHeaderText("解锁失败");
		//alert.setHeaderText(null);
		alert.setContentText("解锁时发生系统错误!");
		alert.showAndWait();
            }catch(ClassNotFoundException ex){
                 Alert alert=new Alert(Alert.AlertType.ERROR);
		alert.setHeaderText("解锁失败");
		//alert.setHeaderText(null);
		alert.setContentText("找不到数据库驱动!");
		alert.showAndWait();
            }
    }
    
}
