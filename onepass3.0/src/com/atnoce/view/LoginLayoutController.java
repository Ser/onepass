/*
 * Copyright 2016 atnoce.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.atnoce.view;


import com.atnoce.Start;
import com.atnoce.core.SecurityController;
import com.atnoce.db.ConfigDao;
import com.atnoce.util.Cache;
import com.atnoce.util.CommonUtil;
import java.sql.SQLException;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.PasswordField;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.stage.Stage;
import org.apache.commons.lang.StringUtils;

public class LoginLayoutController {

    @FXML
    private PasswordField mainPass;
    @FXML
    private Button loginBtn;
    @FXML
    private ImageView loginimage;
    @FXML
    private Hyperlink exitLink;
    private Start start;
    private Stage stage;
    private String coludAccount;
    private final ConfigDao cd=new ConfigDao();
    public void setStart(Start start){
	this.start=start;
    }
    public void setColudAccount(String coludAccount){
        this.coludAccount=coludAccount;
    }
    public void setStage(Stage stage){
        this.stage=stage;
          //  this.stage.resizableProperty().setValue(Boolean.TRUE);
    }
    @FXML
    private void initialize(){
        mainPass.setTooltip(CommonUtil.getTooltip("在此处输入主密码", 14));
        loginBtn.setTooltip(CommonUtil.getTooltip("点击进入", 14));
	loginBtn.setOnAction(e->login());
        //响应登录框中的回车事件
        mainPass.setOnKeyPressed(e->{
            if(e.getCode()==KeyCode.ENTER){
		login();
            }
	});
        exitLink.setOnAction(e->exitSystem());
    }
    private void exitSystem(){
//        TrayNotification tray=new TrayNotification();
//        tray.setTitle("onepass提示");
//        tray.setMessage("密码库已同步完成!");
//        tray.setNotificationType(NotificationType.SUCCESS);
//        tray.showAndDismiss(Duration.seconds(3));
        Platform.exit();
    }
    private void login(){
        //Platform.runLater(() -> {
            Alert alert1=new Alert(AlertType.INFORMATION);
            alert1.setHeaderText(null);
        ButtonType get = alert1.getButtonTypes().get(0);
            alert1.getButtonTypes().remove(get);
            alert1.setContentText("正在登录");
            //alert1.initModality(Modality.WINDOW_MODAL);
            //alert1.initStyle(StageStyle.UNDECORATED);
            alert1.show();
            
            String pass = mainPass.getText();
//            if(StringUtils.isNotBlank(Cache.key)){
//                alert1.getButtonTypes().add(get);
//                alert1.close();
//                return;
//            }
            try {
                Cache.oc=cd.getOnepassConfig();
                if(SecurityController.getSecurityInstence().checkMainPass(pass)){
                     start.initMainLayout();
                     alert1.getButtonTypes().add(get);
                    alert1.close();
                    return;
                }else{
                    alert1.getButtonTypes().add(get);
                    alert1.close();
                    Alert alert=new Alert(AlertType.ERROR);
                    alert.setTitle("登录出错");
                    alert.setHeaderText(null);
                    alert.setContentText("输入的密码有误!");
                    alert.showAndWait();
                }
            } catch (SQLException ex) {
                alert1.getButtonTypes().add(get);
                alert1.close();
                Alert alert=new Alert(AlertType.ERROR);
                alert.setHeaderText("登录出错");
                alert.setContentText("登录时发生系统错误!");
                alert.showAndWait();
            }catch(ClassNotFoundException ex){
                alert1.getButtonTypes().add(get);
                alert1.close();
                Alert alert=new Alert(AlertType.ERROR);
                alert.setHeaderText("登录出错");
                alert.setContentText("找不到数据库驱动!");
                alert.showAndWait();
            }
            
        //});
    }
    
}
