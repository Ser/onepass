/*
 * Copyright 2016 atnoce.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.atnoce.pojo;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * 系统配置实体类
 * @author atnoce.com
 */
public class OnepassConfig {
    private final StringProperty strQuanZhong;
	private final StringProperty xiaoXieStrQuanZhong;
	private final StringProperty mainPass;
	private final StringProperty numQuanZhong;
	private final StringProperty fuhaoQuanZhong;
	private final StringProperty filterStr;
	private final StringProperty passLength;
	private final StringProperty startAutoUpdate;
	private final StringProperty minSysTuoPan;
	private final StringProperty closeMinWindowNoClose;
	private final StringProperty autoClearPanel;
	private final StringProperty autoClearPanelTime;
	private final StringProperty minningLock;
	private final StringProperty kongXianLock;
	private final StringProperty kongXianLockTime;
        private final StringProperty coludAccount;
	public OnepassConfig(){
		strQuanZhong=new SimpleStringProperty();
		xiaoXieStrQuanZhong=new SimpleStringProperty();
		mainPass=new SimpleStringProperty();
		numQuanZhong=new SimpleStringProperty();
		fuhaoQuanZhong=new SimpleStringProperty();
		filterStr=new SimpleStringProperty();
		passLength=new SimpleStringProperty();
		startAutoUpdate=new SimpleStringProperty();
		minSysTuoPan=new SimpleStringProperty();
		closeMinWindowNoClose=new SimpleStringProperty();
		autoClearPanel=new SimpleStringProperty();
		autoClearPanelTime=new SimpleStringProperty();
		minningLock=new SimpleStringProperty();
		kongXianLock=new SimpleStringProperty();
		kongXianLockTime=new SimpleStringProperty();
                coludAccount=new SimpleStringProperty();
	}

	public final StringProperty strQuanZhongProperty() {
		return this.strQuanZhong;
	}

	public final String getStrQuanZhong() {
		return this.strQuanZhongProperty().get();
	}

	public final void setStrQuanZhong(final String strQuanZhong) {
		this.strQuanZhongProperty().set(strQuanZhong);
	}

	public final StringProperty mainPassProperty() {
		return this.mainPass;
	}

	public final String getMainPass() {
		return this.mainPassProperty().get();
	}

	public final void setMainPass(final String mainPass) {
		this.mainPassProperty().set(mainPass);
	}

	public final StringProperty numQuanZhongProperty() {
		return this.numQuanZhong;
	}

	public final String getNumQuanZhong() {
		return this.numQuanZhongProperty().get();
	}

	public final void setNumQuanZhong(final String numQuanZhong) {
		this.numQuanZhongProperty().set(numQuanZhong);
	}

	public final StringProperty fuhaoQuanZhongProperty() {
		return this.fuhaoQuanZhong;
	}

	public final String getFuhaoQuanZhong() {
		return this.fuhaoQuanZhongProperty().get();
	}

	public final void setFuhaoQuanZhong(final String fuhaoQuanZhong) {
		this.fuhaoQuanZhongProperty().set(fuhaoQuanZhong);
	}
	public final StringProperty filterStrProperty() {
		return this.filterStr;
	}

	public final String getFilterStr() {
		return this.filterStrProperty().get();
	}

	public final void setFilterStr(final String filterStr) {
		this.filterStrProperty().set(filterStr);
	}
	public final StringProperty passLengthProperty() {
		return this.passLength;
	}

	public final String getPassLength() {
		return this.passLengthProperty().get();
	}

	public final void setPassLength(final String passLength) {
		this.passLengthProperty().set(passLength);
	}
	public final StringProperty xiaoXieStrQuanZhongProperty() {
		return this.xiaoXieStrQuanZhong;
	}

	public final String getXiaoXieStrQuanZhong() {
		return this.xiaoXieStrQuanZhongProperty().get();
	}

	public final void setXiaoXieStrQuanZhong(final String xiaoXieStrQuanZhong) {
		this.xiaoXieStrQuanZhongProperty().set(xiaoXieStrQuanZhong);
	}

	public final StringProperty startAutoUpdateProperty() {
		return this.startAutoUpdate;
	}


	public final String getStartAutoUpdate() {
		return this.startAutoUpdateProperty().get();
	}


	public final void setStartAutoUpdate(final String startAutoUpdate) {
		this.startAutoUpdateProperty().set(startAutoUpdate);
	}


	public final StringProperty minSysTuoPanProperty() {
		return this.minSysTuoPan;
	}


	public final String getMinSysTuoPan() {
		return this.minSysTuoPanProperty().get();
	}


	public final void setMinSysTuoPan(final String minSysTuoPan) {
		this.minSysTuoPanProperty().set(minSysTuoPan);
	}


	public final StringProperty closeMinWindowNoCloseProperty() {
		return this.closeMinWindowNoClose;
	}


	public final String getCloseMinWindowNoClose() {
		return this.closeMinWindowNoCloseProperty().get();
	}


	public final void setCloseMinWindowNoClose(final String closeMinWindowNoClose) {
		this.closeMinWindowNoCloseProperty().set(closeMinWindowNoClose);
	}


	public final StringProperty autoClearPanelProperty() {
		return this.autoClearPanel;
	}


	public final String getAutoClearPanel() {
		return this.autoClearPanelProperty().get();
	}


	public final void setAutoClearPanel(final String autoClearPanel) {
		this.autoClearPanelProperty().set(autoClearPanel);
	}


	public final StringProperty autoClearPanelTimeProperty() {
		return this.autoClearPanelTime;
	}


	public final String getAutoClearPanelTime() {
		return this.autoClearPanelTimeProperty().get();
	}


	public final void setAutoClearPanelTime(final String autoClearPanelTime) {
		this.autoClearPanelTimeProperty().set(autoClearPanelTime);
	}


	public final StringProperty minningLockProperty() {
		return this.minningLock;
	}


	public final String getMinningLock() {
		return this.minningLockProperty().get();
	}


	public final void setMinningLock(final String minningLock) {
		this.minningLockProperty().set(minningLock);
	}


	public final StringProperty kongXianLockProperty() {
		return this.kongXianLock;
	}


	public final String getKongXianLock() {
		return this.kongXianLockProperty().get();
	}


	public final void setKongXianLock(final String kongXianLock) {
		this.kongXianLockProperty().set(kongXianLock);
	}


	public final StringProperty kongXianLockTimeProperty() {
		return this.kongXianLockTime;
	}


	public final String getKongXianLockTime() {
		return this.kongXianLockTimeProperty().get();
	}


	public final void setKongXianLockTime(final String kongXianLockTime) {
		this.kongXianLockTimeProperty().set(kongXianLockTime);
	}
        public final StringProperty coludAccountProperty() {
		return this.coludAccount;
	}

	public final String getColudAccount() {
		return this.coludAccountProperty().get();
	}
        public final void setColudAccount(final String coludAccount){
            this.coludAccountProperty().set(coludAccount);
        }
}
