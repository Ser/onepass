/*
 * Copyright 2016 atnoce.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.atnoce.pojo;

import com.atnoce.core.CommonCore;
import java.time.LocalDate;
import java.util.Calendar;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author Administrator
 */
public class DirType {
    

	private final StringProperty dirId;
	private final StringProperty dirName;
	private final ObjectProperty<LocalDate> createTime;

	public DirType(String dirName){
		this.dirId=new SimpleStringProperty(CommonCore.getId());
		this.dirName=new SimpleStringProperty(dirName);
		Calendar date=Calendar.getInstance();
		this.createTime=new SimpleObjectProperty<>(LocalDate.of(date.get(Calendar.YEAR),
				date.get(Calendar.MONTH), date.get(Calendar.DAY_OF_MONTH)));
	}
	public DirType(String dirId,String dirName,LocalDate createTime){
		this.dirId=new SimpleStringProperty(dirId);
		this.dirName=new SimpleStringProperty(dirName);
		this.createTime=new SimpleObjectProperty<>(createTime);
	}
	public String getDirId(){
		return dirId.get();
	}
	public void setDirId(String dirId){
		this.dirId.set(dirId);
	}
	public StringProperty dirIdProperty(){
		return dirId;
	}
	public String getDirName(){
		return dirName.get();
	}
	public void setDirName(String dirName){
		this.dirName.set(dirName);
	}
	public StringProperty dirNameProperty(){
		return dirName;
	}
	public LocalDate getCreateTime(){
		return createTime.get();
	}
	public void setCreateTime(LocalDate createTime){
		this.createTime.set(createTime);
	}
	public ObjectProperty<LocalDate> createTimeProperty(){
		return createTime;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return dirName.get();
	}
}
