/*
 * Copyright 2016 atnoce.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.atnoce.pojo;

import com.atnoce.core.CommonCore;
import java.time.LocalDate;
import java.util.Calendar;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author Administrator
 */
public class PasswordItem {
    

	private final StringProperty passId;
	private final StringProperty account;
	private final StringProperty passwordStr;
	private final StringProperty remarkStr;
	private final ObjectProperty<LocalDate> createTime;
	private final StringProperty isDelete;
	private final StringProperty dirTypeId;

	public PasswordItem(){
		passId=new SimpleStringProperty();
		account=new SimpleStringProperty();
		passwordStr=new SimpleStringProperty();
		remarkStr=new SimpleStringProperty();
		createTime=new SimpleObjectProperty<>();
		isDelete=new SimpleStringProperty();
		dirTypeId=new SimpleStringProperty();
	}

	public PasswordItem(String account,String passwordStr,String remarkStr,String dirTypeId){
		this.passId=new SimpleStringProperty(CommonCore.getId());
		this.account=new SimpleStringProperty(account);
		this.passwordStr=new SimpleStringProperty(passwordStr);
		this.remarkStr=new SimpleStringProperty(remarkStr);
		Calendar calendar=Calendar.getInstance();
		this.createTime=new SimpleObjectProperty<>(LocalDate.of(calendar.get(Calendar.YEAR),
				calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH)));
		isDelete=new SimpleStringProperty("false");
		this.dirTypeId=new SimpleStringProperty(dirTypeId);
	}
	public PasswordItem(String passId,String account,String passwordStr,String remarkStr,String dirTypeId,LocalDate createTime,String isDelete){
		this.passId=new SimpleStringProperty(passId);
		this.account=new SimpleStringProperty(account);
		this.passwordStr=new SimpleStringProperty(passwordStr);
		this.remarkStr=new SimpleStringProperty(remarkStr);
		this.createTime=new SimpleObjectProperty<>(createTime);
		this.isDelete=new SimpleStringProperty(isDelete);
		this.dirTypeId=new SimpleStringProperty(dirTypeId);
	}

	public  StringProperty passIdProperty() {
		return this.passId;
	}

	public  String getPassId() {
		return this.passIdProperty().get();
	}

	public  void setPassId( String passId) {
		this.passIdProperty().set(passId);
	}

	public  StringProperty accountProperty() {
		return this.account;
	}

	public  String getAccount() {
		return this.accountProperty().get();
	}

	public  void setAccount(String account) {
		this.accountProperty().set(account);
	}

	public  StringProperty passwordStrProperty() {
		return this.passwordStr;
	}

	public  String getPasswordStr() {
		return this.passwordStrProperty().get();
	}

	public  void setPasswordStr( String passwordStr) {
		this.passwordStrProperty().set(passwordStr);
	}

	public  StringProperty remarkStrProperty() {
		return this.remarkStr;
	}

	public  String getRemarkStr() {
		return this.remarkStrProperty().get();
	}

	public  void setRemarkStr( String remarkStr) {
		this.remarkStrProperty().set(remarkStr);
	}

	public  ObjectProperty<LocalDate> createTimeProperty() {
		return this.createTime;
	}

	public  LocalDate getCreateTime() {
		return this.createTimeProperty().get();
	}

	public  void setCreateTime( LocalDate createTime) {
		this.createTimeProperty().set(createTime);
	}

	public  StringProperty isDeleteProperty() {
		return this.isDelete;
	}

	public  String getIsDelete() {
		return this.isDeleteProperty().get();
	}

	public  void setIsDelete( String isDelete) {
		this.isDeleteProperty().set(isDelete);
	}

	public  StringProperty dirTypeIdProperty() {
		return this.dirTypeId;
	}

	public  String getDirTypeId() {
		return this.dirTypeIdProperty().get();
	}

	public  void setDirTypeId( String dirTypeId) {
		this.dirTypeIdProperty().set(dirTypeId);
	}
}
