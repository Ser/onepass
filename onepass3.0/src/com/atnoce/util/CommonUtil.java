/*
 * Copyright 2016 atnoce.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.atnoce.util;

import com.atnoce.net.BasicNetService;
import java.io.IOException;
import java.time.format.DateTimeFormatter;
import javafx.scene.control.Tooltip;
import javafx.scene.text.Font;

/**
 * 工具类
 * @author atnoce.com
 */
public class CommonUtil {
    public final static String APP_NAME="onepass";
        public final static String APP_VERSION="1.0.5";
        public final static String DEFAULT_DB_PATH="./onepass.db";
        public final static String COLUD_ACCOUNT_KEY="BC99ED5371B5162EFC9D071B5215F735907026A2";
        
        public final static int CONNECTION_TIMEOUT=5000;

	public static DateTimeFormatter getDateTimeFormatter(){
		return DateTimeFormatter.ofPattern("uuuu-MM-dd");
	}
        public static Tooltip getTooltip(String context,int size){
            Tooltip tip=new Tooltip();
            tip.setText(context);
            tip.setFont(new Font("Arial", size));
            return tip;
        }
        /**
         * 检查账号格式
         * @param account
         * @return 
         */
        public static String checkCloudAccount(String account){
            String result="";
            //if()
            
            return result;
        }
        public static void updateSyncState(){
            try {
                InitContext.init();//刷新配置信息
                if(!Boolean.valueOf((String)Cache.proMap.get("localupdate"))){
                    long time = BasicNetService.getTime();
                    InitContext.updateProperties("localupdate", "true");
                    InitContext.updateProperties("localtime", time+"");
                }
            } catch (IOException ex) {
            }
        }
}
