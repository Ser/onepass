/*
 * Copyright 2016 atnoce.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.atnoce.util;

import com.atnoce.pojo.OnepassConfig;
import java.util.HashMap;
import java.util.Map;

/**
 * 系统高速缓存类
 * @author atnoce.com
 */
public class Cache {
    public static Map<String,String> proMap=new HashMap<>();
    private static String DB_PATH="";
    public static String key="";
     /**
     * 系统配置信息
     */
    public static OnepassConfig oc;
    /**
     * 新增密码面板中被选中的分类
     */
    public static String ADD_PASS_ITEM_SELECT_DIRTYPE=null;
    /**
     * 主面板左侧分类列表中被选中的分类
     */
    public static String INDEX_LEFT_SELECTED_DIRTYPE=null; 
    
    public static String getDbPaht(){
        return "d:\\openproject";
//        String thisPath = "D:\\vcredist.bmp";  
//        String str1=thisPath.substring(0,1).toLowerCase();  //直接将字符串第一个字母小写  
//        String str2=thisPath.substring(1,thisPath.length());//截取字符串第二个以后  
//        thisPath=str1+str2;  
//        return thisPath;
    }
}
