/*
 * Copyright 2016 Administrator.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.atnoce.db;

import com.atnoce.core.SecurityController;
import com.atnoce.pojo.PasswordItem;
import com.atnoce.util.Cache;
import com.atnoce.util.CommonUtil;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author Administrator
 */
public class CommonDao {
    public ObservableList<PasswordItem> search(String str) throws SQLException, ClassNotFoundException, Exception{
        PreparedStatement  ps=DbUtils.getDBConnection().prepareStatement("select * from passitem;");
        ResultSet query = ps.executeQuery();
        ObservableList<PasswordItem> oblist=FXCollections.observableArrayList();
        ObservableList<PasswordItem> rblist=FXCollections.observableArrayList();
        while(query.next()){
            String id=query.getString("passId");
            String account=query.getString("account");
            String pass=query.getString("passwordStr");
            String remarkStr=query.getString("remarkStr");
            String createTime=query.getString("createTime");
            String isDelete=query.getString("isDelete");
            String dirTypeId=query.getString("dirTypeId");
            oblist.add(new PasswordItem(id,account, pass, remarkStr, dirTypeId,
			LocalDate.parse(createTime,CommonUtil.getDateTimeFormatter()), isDelete));
        }
        if(!oblist.isEmpty()){
            if(StringUtils.isBlank(str)){
                return oblist;
            }
            for(PasswordItem pi:oblist){
                if(SecurityController.getSecurityInstence().AESdecrypt(pi.getAccount(), Cache.key).contains(str)||
                        pi.getRemarkStr().contains(str)){
                    
                    rblist.add(pi);
                }
            }
        }
        return rblist;
    }
}
