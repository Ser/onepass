/*
 * Copyright 2016 atnoce.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.atnoce.db;

import com.atnoce.pojo.DirType;
import com.atnoce.util.CommonUtil;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author Administrator
 */
public class ClassTypeDao {
    
	private Statement statement=null;
	DateTimeFormatter f = DateTimeFormatter.ofPattern("uuuu-MM-dd HH:mm:ss");
	private void initStatement() throws SQLException, ClassNotFoundException{
		if(statement==null){
			statement=DbUtils.getDBConnection().createStatement();
		}
	}
        /**
         * 获取所有分类
         * @return
         * @throws SQLException 
         */
	public ObservableList<DirType> getDirTypes() throws SQLException, ClassNotFoundException{
		initStatement();
		ResultSet executeQuery = statement.executeQuery("select * from fenlei;");
		ObservableList<DirType> oblist=FXCollections.observableArrayList();
		while(executeQuery.next()){
			String id=executeQuery.getString("dirId");
			String name=executeQuery.getString("dirName");
			oblist.add(new DirType(id, name, LocalDate.parse(executeQuery.getString("createTime"),
					CommonUtil.getDateTimeFormatter())));
		}
		return oblist;
	}
        /**
         * 添加分类
         * @param dirType
         * @return
         * @throws SQLException 
         */
	public Map<String,Object> addDirType(DirType dirType) throws SQLException, ClassNotFoundException{
            Map<String,Object> map=new HashMap<>();
		initStatement();
            PreparedStatement ps=DbUtils.getDBConnection().prepareStatement("select * from fenlei where dirName=?;");
            ps.setString(1, dirType.getDirName());
            ResultSet executeQuery = ps.executeQuery();
            if(executeQuery.next()){
                executeQuery.close();
                ps.close();
                map.put("flag", false);
                map.put("msg", "分类已存在");
                return map;
            }
                ps.close();
		 ps=DbUtils.getDBConnection().prepareStatement("insert into fenlei values (?,?,?);");
		ps.setString(1, dirType.getDirId());
		ps.setString(2, dirType.getDirName());
		ps.setString(3, dirType.getCreateTime().toString());
		//如果第一个结果是 ResultSet 对象，则返回 true；如果第一个结果是更新计数或者没有结果，则返回 false
		ps.execute();
		int updateCount = ps.getUpdateCount();
                if(updateCount==1){
                    map.put("flag", true);
                    CommonUtil.updateSyncState();
                }
		return map;
	}
        /**
         * 删除分类
         * @param dirName
         * @return
         * @throws SQLException 
         */
        public boolean deleteDirType(String dirName) throws SQLException, ClassNotFoundException{
            PreparedStatement ps=DbUtils.getDBConnection().prepareStatement("delete from fenlei where dirName=?;");
            ps.setString(1, dirName);
            ps.execute();
            int updateCount = ps.getUpdateCount();
            if(updateCount==1){
                CommonUtil.updateSyncState();
                return true;
            }
            return false;
        }
        /**
         * 重命名分类名称
         * @param oldName
         * @param newName
         * @return
         * @throws SQLException 
         */
        public boolean renameDirType(String oldName,String newName) throws SQLException, ClassNotFoundException{
            PreparedStatement ps=DbUtils.getDBConnection().prepareStatement("update fenlei set dirName=? where dirName=?;");
            ps.setString(1, newName);
            ps.setString(2, oldName);
            ps.executeUpdate();
            int updateCount = ps.getUpdateCount();
            if(updateCount==1){
                CommonUtil.updateSyncState();
                return true;
            }
            return false;
        }
}
