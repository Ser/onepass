/*
 * Copyright 2016 atnoce.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.atnoce.db;

import com.atnoce.pojo.OnepassConfig;
import com.atnoce.util.CommonUtil;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author Administrator
 */
public class ConfigDao {
    
	private Statement statement=null;
	private void initStatement() throws SQLException, ClassNotFoundException{
		if(statement==null){
			statement=DbUtils.getDBConnection().createStatement();
		}
	}
        /**
         * 获取配置信息
         * @return
         * @throws SQLException 
         */
	public OnepassConfig getOnepassConfig() throws SQLException, ClassNotFoundException{
		initStatement();
		ResultSet executeQuery = statement.executeQuery("select * from onepassconfig");
		if(executeQuery.next()){
			OnepassConfig oc=new OnepassConfig();
			oc.setStrQuanZhong(executeQuery.getString("strQuanZhong"));
			oc.setXiaoXieStrQuanZhong(executeQuery.getString("xiaoXieStrQuanZhong"));
			oc.setNumQuanZhong(executeQuery.getString("numQuanZhong"));
			oc.setFuhaoQuanZhong(executeQuery.getString("fuhaoQuanZhong"));
			oc.setMainPass(executeQuery.getString("mainPass"));
			oc.setFilterStr(executeQuery.getString("filterStr"));
			oc.setPassLength(executeQuery.getString("passLength"));
			oc.setStartAutoUpdate(executeQuery.getString("startAutoUpdate"));
			oc.setMinSysTuoPan(executeQuery.getString("minSysTuoPan"));
			oc.setCloseMinWindowNoClose(executeQuery.getString("closeMinWindowNoClose"));
			oc.setAutoClearPanel(executeQuery.getString("autoClearPanel"));
			oc.setAutoClearPanelTime(executeQuery.getString("autoClearPanelTime"));
			oc.setMinningLock(executeQuery.getString("minningLock"));
			oc.setKongXianLock(executeQuery.getString("kongXianLock"));
			oc.setKongXianLockTime(executeQuery.getString("kongXianLockTime"));
                        oc.setColudAccount(executeQuery.getString("coludAccount"));
			return oc;
		}
		return null;
	}
        /**
         * 保存密码生成器配置
         * @param oc
         * @return
         * @throws SQLException 
         */
	public boolean savePassGenerConfig(OnepassConfig oc) throws SQLException, ClassNotFoundException{
		initStatement();
		PreparedStatement ps=DbUtils.getDBConnection().prepareStatement(
				"update onepassconfig set strQuanZhong=?,numQuanZhong=?,fuhaoQuanZhong=?,filterStr=?,passLength=?,xiaoXieStrQuanZhong=?");
		ps.setString(1, oc.getStrQuanZhong());
		ps.setString(2, oc.getNumQuanZhong());
		ps.setString(3, oc.getFuhaoQuanZhong());
		ps.setString(4, oc.getFilterStr());
		ps.setString(5, oc.getPassLength());
		ps.setString(6, oc.getXiaoXieStrQuanZhong());
		ps.execute();
		int updateCount = ps.getUpdateCount();
		if(updateCount==1){
                    CommonUtil.updateSyncState();
                    return true;
                }
                return false;
	}
        /**
         * 保存常规配置
         * @param oc
         * @return
         * @throws SQLException 
         */
	public boolean saveChangGuiConfig(OnepassConfig oc) throws SQLException, ClassNotFoundException{
		initStatement();
		PreparedStatement ps=DbUtils.getDBConnection().prepareStatement(
				"update onepassconfig set startAutoUpdate=?,minSysTuoPan=?,closeMinWindowNoClose=?,autoClearPanel=?,autoClearPanelTime=?,"
				+ "minningLock=?,kongXianLock=?,kongXianLockTime=?");
		ps.setString(1, oc.getStartAutoUpdate());
		ps.setString(2, oc.getMinSysTuoPan());
		ps.setString(3, oc.getCloseMinWindowNoClose());
		ps.setString(4, oc.getAutoClearPanel());
		ps.setString(5, oc.getAutoClearPanelTime());
		ps.setString(6, oc.getMinningLock());
		ps.setString(7, oc.getKongXianLock());
		ps.setString(8, oc.getKongXianLockTime());
		ps.execute();
		int updateCount = ps.getUpdateCount();
		if(updateCount==1){
                    CommonUtil.updateSyncState();
                    return true;
                }
                return false;
	}
        /**
         * 更新主密码
         * @param pass
         * @return
         * @throws SQLException 
         */
        public boolean updateMainPass(String pass) throws SQLException, ClassNotFoundException{
            PreparedStatement ps=DbUtils.getDBConnection().prepareStatement("update onepassconfig set mainPass=?");
            ps.setString(1, pass);
            int executeUpdate = ps.executeUpdate();
            if(executeUpdate==1){
                CommonUtil.updateSyncState();
                return true;
            }
            return false;
        }
        /**
         * 设置主密码
         * @param coludAccount
         * @return
         * @throws SQLException 
         */
        @Deprecated
        public boolean regColudAccount(String coludAccount) throws SQLException, ClassNotFoundException{
            PreparedStatement ps=DbUtils.getDBConnection().prepareStatement("update onepassconfig set coludAccount=?");
            ps.setString(1, coludAccount);
            int executeUpdate = ps.executeUpdate();
            if(executeUpdate==1){
                CommonUtil.updateSyncState();
                return true;
            }
            return false;
        }
}
