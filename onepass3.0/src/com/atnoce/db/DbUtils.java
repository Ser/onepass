/*
 * Copyright 2016 atnoce.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.atnoce.db;

import com.atnoce.util.Cache;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author Administrator
 */
public class DbUtils {
    private static Connection conn=null;
	private static Statement stat=null;
	private static DatabaseMetaData meta=null;
	
        private static void openAll() throws ClassNotFoundException, SQLException{
            if(conn==null){
		Class.forName("org.sqlite.JDBC");
                if(StringUtils.isBlank(Cache.proMap.get("otherdbdir"))){
                    conn=DriverManager.getConnection("jdbc:sqlite:onepass.db");
                }else{
                    conn=DriverManager.getConnection("jdbc:sqlite://"+Cache.getDbPaht().replace("\\", "/")+"/onepass.db");
                }
            }
            if(stat==null){
		stat=conn.createStatement();
            }
            if(meta==null){
		meta=conn.getMetaData();
            }
        }
        public static void closeAll() throws SQLException{
            if(conn!=null){
                conn.close();
                conn=null;
            }
        }
	public static void initTables() throws ClassNotFoundException, SQLException{
            openAll();
            ResultSet table=null;
            table = meta.getTables(null, null, "passitem", null);
            if(!table.next()){
                stat.executeUpdate("create table passitem (passId text, account text,passwordStr char(500),remarkStr char(500),"
                    + "createTime char(50),isDelete char(5),dirTypeId char(50));");
            }
            table = meta.getTables(null, null, "fenlei", null);
            if(!table.next()){
                stat.executeUpdate("create table fenlei (dirId char(50), dirName char(50),createTime char(50));");
            }
            table = meta.getTables(null, null, "onepassconfig", null);
            if(!table.next()){
		stat.executeUpdate("CREATE TABLE onepassconfig (xiaoXieStrQuanZhong  char(10),strQuanZhong  char(2),numQuanZhong  char(2),fuhaoQuanZhong  char(2),mainPass  char(512),filterStr  char(10),passLength char(10),startAutoUpdate  char(10),minSysTuoPan  char(10),closeMinWindowNoClose  char(10),autoClearPanel char(10),autoClearPanelTime  char(10),minningLock  char(10),kongXianLock  char(10),kongXianLockTime  char(10),coludAccount char(256));");
                stat.execute("insert into onepassconfig values ('3','1','1','2','44f20875d8956a6650cc1ab0b70b87680979464b46d23d955cfb8e6c7f9016a66850e4e20b3e7a5de713051d01f59a1473bd1f68d74c328e5ec0c7346179b5c0','true','8','false','true','true','true','30','true','true','1200',NULL);");
            }
	}
	public static Connection getDBConnection() throws ClassNotFoundException, SQLException{
             if(conn==null){
                openAll();
            }
		return conn;
	}
}
