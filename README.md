#onepass
---- 
 **onepass** 是一个基于javaFX创建的密码管理工具，类似于1Password、keepass等，主要用来管理日益增多的个人密码。   

 **onepass** 使用JavaFX实现，在学习JavaFX的同时顺便做一个可以使用的小工具。 

 **onepass** 共有 **n、m** 两个版本：    
-  **onepass-n**  使用原生javaFX技术创建的本地客户端应用，布局文件主要是fxml+css。   
-  **onepass-m**  是onepass的移动应用版本。

    

这个项目其实我已经在自己电脑上用了，用的感觉还算不错，但是也有一些小的问题需要优化，也有一些必须的功能还没有添加上去，总体来说使用是没有问题的。   
由于我本机是window系统，所以我只编译打包了window系统下的可执行程序，打包过程详见我的个人博客：   

现在决定将此项目开源，并且逐步完善一些小功能。    

[点击这里可下载已经打包好的可执行jar文件](http://git.oschina.net/softxj/onepass/attach_files)

##onepass4.0  
onepass4.0是最近才发起的一个新版本的迭代，其中最大的变化就是同步方式从文件同步更改为记录同步，为客户端的开发铺路，这样一来，就可以完美的实现移动端、PC端无缝同步数据。

#####使用建议   
- 不可粘贴密码的应用、网站不建议使用此工具    

#####现有密码管理工具存在的问题 
- 1password收费高昂，并且不是全平台支持或者全平台支持不友好。  
- keepass仅支持本地使用，不能在多台电脑之间同步密码库，如果想要在多个电脑上使用同一个密码库需要使用其他方法，如：使用移动介质拷贝密码库或者使用云盘同步密码库。并且在手机端与电脑端同步密码库较困难。
- ......

####onepass特点


- 界面简洁易用，没有多余的设置或功能
- 提供云服务能力，可以根据需要，自己决定是否需要云服务同步密码库
- 密码库安全性较高，无须担心密码库安全，即便密码库被拷贝，没有主密码也无法看到密码信息  

####onepass设计原理
在第一次打开onepass的时候会要求设置一个主密码，设置的主密码会通过 **sha512** 加密算法加密 **100次** 后将密文存储于密码库中。    
以后打开密码库的时候会要求输入主密码，通过将输入的主密码 **sha512** 加密 **100次** 后得到密文，用该密文与密码库中存储的主密码密文对比判定主密码的正确性。    
在主密码验证通过后会将主密码原文用 **sha512** 加密算法运算 **1000次** 后得到 **临时秘钥K** 。    
在添加密码的时候，将密码项的用户名和密码使用 **AES** 加密算法进行加密 **1000次** (加密次数可在工具中配置)，并且使用临时秘钥 **K 作为AES加密密码项的key** 。    

所以用onepass保存的密码非常安全，如果主密码一旦遗忘，那么也将导致密码库无法打开，这是必然的。   


####软件截图    
![输入图片说明](http://git.oschina.net/uploads/images/2016/0930/003948_4f725990_470721.png "在这里输入图片标题")
![输入图片说明](http://git.oschina.net/uploads/images/2016/0930/004036_c3e4039a_470721.png "在这里输入图片标题")
![输入图片说明](http://git.oschina.net/uploads/images/2016/0930/004115_4cb36073_470721.png "在这里输入图片标题")
![输入图片说明](http://git.oschina.net/uploads/images/2016/0930/004224_89079bb3_470721.png "在这里输入图片标题")
![输入图片说明](http://git.oschina.net/uploads/images/2016/0930/004307_125a4a2a_470721.png "在这里输入图片标题")
![输入图片说明](http://git.oschina.net/uploads/images/2016/0930/004342_d3ad5212_470721.png "在这里输入图片标题")
![输入图片说明](http://git.oschina.net/uploads/images/2016/0930/004444_f75f5233_470721.png "在这里输入图片标题")
![输入图片说明](http://git.oschina.net/uploads/images/2016/0930/004532_ea3cf34c_470721.png "在这里输入图片标题")
![输入图片说明](http://git.oschina.net/uploads/images/2016/0930/004643_d7693f1a_470721.png "在这里输入图片标题")
![输入图片说明](http://git.oschina.net/uploads/images/2016/0930/004733_8669366e_470721.png "在这里输入图片标题")
![输入图片说明](http://git.oschina.net/uploads/images/2016/0930/004809_7c4a9b1c_470721.png "在这里输入图片标题")